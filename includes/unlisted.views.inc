<?php

/**
 * Implements hook_views_data().
 */
function unlisted_views_data() {
  $data['unlisted']['table']['group'] = t('Unlisted');
  $data['unlisted']['unlisted'] = array(
    'title' => t('Unlisted'),
    'help' => t('Whether the node is unlisted.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'unlisted-not-unlisted' => array(t('Unlisted'), t('Listed')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Unlisted node'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['unlisted']['table']['join'] = array(
    'node' => array(
      'type' => 'INNER',
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  return $data;
}
